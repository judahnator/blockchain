<?php

namespace judahnator\BlockChain;

if (!function_exists('createOriginBlock')) {
    /**
     * Creates an origin block.
     *
     * @param \stdClass $blockData
     * @param \DateTime|null $created_at
     * @return Block
     */
    function createOriginBlock(\stdClass $blockData, \DateTime $created_at = null): Block
    {
        return BlockChain::create($blockData, null, $created_at);
    }
}
