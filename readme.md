Simple BlockChain Library
=========================

This library is fairly straightforward. Setup is equally as easy.

Just run `composer require judahnator/blockchain` in your projects directory
and away you go!

Optional Configuration
----------------------

This library works out of the box just fine. You might, however, desire to 
use a different BlockChain driver than the default in-memory driver, you 
will have to call the `BlockChain::load()` function call passing in the
desired driver class.

Once you pass the driver once, you will no longer need to do so. You can
change the driver you use by passing in a new one though.

Here is a quick example of how to do that:

```php
<?php

use judahnator\BlockChain\BlockChain;
use judahnator\BlockChain\Drivers\FileDriver;

// Tell the application to use the FileDriver class
BlockChain::load(new FileDriver());

// future calls do not require passing in the driver class
BlockChain::load();
BlockChain::originBlock();
```

Usage
-----

The `BlockChain` class is useful because it will help you find the
origin block.

`BlockChain::originBlock()`

So now you know how to retrieve blocks from the chain, but how to use them?

Each block has the following properties:

* created_at
    * \DateTime object
* data
    * The data object (\stdClass)
* height
    * The number of blocks removed from the origin block
* hash
    * The blocks hash
* previous
    * The parent block
    * Returns null on the origin block
* orphaned
    * boolean value, true only when an ancestor block is missing or invalid
    
You can find a block by its hash:

`BlockChain::find($blockHash)`

You can create a child for a given block by doing this:

`BlockChain::find($blockHash)->createChild((object)['foo' => 'bar'])`