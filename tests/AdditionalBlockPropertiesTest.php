<?php

namespace judahnator\BlockChain\Tests;


use judahnator\BlockChain\Block;
use judahnator\BlockChain\BlockChain;
use PHPUnit\Framework\TestCase;

class AdditionalBlockPropertiesTest extends TestCase
{

    public function testSumAdditionalProperty() {
        Block::addProperty('sum', function(Block $block) {
            return array_sum((array)$block->data);
        });
        $blockToTest = BlockChain::originBlock()->createChild((object)[1,2,3]);
        $this->assertEquals(6, $blockToTest->sum);
    }

    public function testCheckingIfPropertyRegistered() {
        $this->assertFalse(Block::hasProperty('foobar'));
        Block::addProperty('foobar', function() {});
        $this->assertTrue(Block::hasProperty('foobar'));
    }

}