<?php

namespace judahnator\BlockChain\Drivers;


use judahnator\BlockChain\Block;
use judahnator\BlockChain\BlockChain;
use function judahnator\BlockChain\blocksPath;
use function judahnator\BlockChain\createOriginBlock;
use judahnator\BlockChain\Exceptions\BlockNotFoundException;
use judahnator\BlockChain\Exceptions\InvalidBlockException;

class FileDriver implements BlockStorageInterface
{

    private $blocksPath;

    public function __construct(string $blocksPath = null)
    {
        $this->blocksPath = $blocksPath ?? realpath(__DIR__.'/../../data/Blocks/');
    }

    public function children(Block $block): array
    {
        $blockFiles = array_diff(scandir($this->blocksPath), array('..', '.', '.gitignore'));

        $childBlocks = [];
        foreach ($blockFiles as $blockFile) {
            $blockData = json_decode(file_get_contents($this->blocksPath.'/'.$blockFile));
            if (
                $blockData->height !== $block->height + 1 ||
                $blockData->previousHash !== $block->hash
            ) {
                continue;
            }
            $childBlocks[] = new Block(
                $blockData->height,
                $block->previous->hash ?? '',
                (new \DateTime())->setTimestamp($blockData->created_at),
                $blockData->data
            );
        }

        return $childBlocks;
    }

    public function delete(Block $block): void
    {
        unlink("{$this->blocksPath}/{$block->hash}.json");
    }

    /**
     * @param string $blockHash
     * @return Block
     * @throws InvalidBlockException
     */
    public function find(string $blockHash): Block
    {
        if (!file_exists("{$this->blocksPath}/{$blockHash}.json")) {
            throw new BlockNotFoundException();
        }

        $blockData = json_decode(file_get_contents("{$this->blocksPath}/{$blockHash}.json"));

        $block = new Block(
            (int)$blockData->height,
            (string)$blockData->previousHash,
            new \DateTime("@{$blockData->created_at}"),
            $blockData->data
        );

        if ($block->hash !== $blockHash) {
            throw new InvalidBlockException($block->hash);
        }

        return $block;
    }

    public function originBlock(\stdClass $defaultData = null): Block
    {
        $blockFiles = array_diff(scandir($this->blocksPath), array('..', '.', '.gitignore'));

        foreach ($blockFiles as $blockFile) {
            $blockData = json_decode(file_get_contents("{$this->blocksPath}/{$blockFile}"));
            if ($blockData->height === 0) {
                return new Block(
                    $blockData->height,
                    $block->previous->hash ?? '',
                    (new \DateTime())->setTimestamp($blockData->created_at),
                    $blockData->data
                );
            }
        }

        return createOriginBlock($defaultData);
    }

    public function save(Block $block): void
    {
        file_put_contents(
            "{$this->blocksPath}/{$block->hash}.json",
            json_encode(
                [
                    'height' => $block->height,
                    'data' => $block->data,
                    'previousHash' => $block->previous->hash ?? '',
                    'created_at' => $block->created_at->getTimestamp()
                ],
                JSON_PRETTY_PRINT
            )
        );
    }

}