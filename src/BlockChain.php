<?php

namespace judahnator\BlockChain;


use judahnator\BlockChain\Drivers\BlockStorageInterface;
use judahnator\BlockChain\Drivers\MemoryDriver;

final class BlockChain
{

    private $driver;

    private function __construct() {}

    private function __clone() {}

    /**
     * Returns the children of a given block.
     *
     * @param Block $block
     * @return array
     */
    public static function children(Block $block): array
    {
        return self::load()->driver->children($block);
    }

    /**
     * Creates a new block as a child of the block passed.
     *
     * @param \stdClass $data
     * @param Block|null $parentBlock
     * @param bool $addToIndex
     * @param \DateTime|null $created_at
     * @return Block
     */
    public static function create(\stdClass $data, Block $parentBlock = null, \DateTime $created_at = null): Block
    {
        if (is_null($created_at)) {
            $created_at = new \DateTime();
        }

        if (!is_null($parentBlock)) {
            if ($parentBlock->created_at->getTimestamp() > $created_at->getTimestamp()) {
                throw new \InvalidArgumentException('Cannot create a block older than its parent');
            }
            $height = $parentBlock->height + 1;
            $previousBlockHash = $parentBlock->hash;
        }else {
            $height = 0;
            $previousBlockHash = '';
        }

        $newBlock = new Block($height, $previousBlockHash, $created_at, $data);

        self::load()->driver->save($newBlock);

        return $newBlock;
    }

    /**
     * Deletes the given block.
     *
     * @param Block $block
     */
    public static function delete(Block $block): void
    {
        self::load()->driver->delete($block);
    }

    public static function find(string $blockHash): Block
    {
        return self::load()->driver->find($blockHash);
    }

    /**
     * Loads the blockchain class singleton.
     *
     * @param BlockStorageInterface $driver
     * @return BlockChain
     */
    public static function load(BlockStorageInterface $driver = null): self
    {
        static $self = null;

        if (is_null($self)) {
            $self = new self();
        }

        if (!is_null($driver)) {
            $self->driver = $driver;
        }
        if (!$self->driver && is_null($driver)) {
            $self->driver = new MemoryDriver();
        }

        return $self;
    }

    /**
     * Gets the origin block.
     *
     * @return Block
     */
    public static function originBlock(\stdClass $defaultData = null): Block
    {
        return self::load()->driver->originBlock($defaultData);
    }

}