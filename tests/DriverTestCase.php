<?php

namespace judahnator\BlockChain\Tests;

use judahnator\BlockChain\BlockChain;
use judahnator\BlockChain\Drivers\BlockStorageInterface;
use PHPUnit\Framework\TestCase;

abstract class DriverTestCase extends TestCase
{

    protected $blockChain;

    protected function setUp()
    {
        if (!$this->blockChain) {
            $this->blockChain = BlockChain::load($this->getDriver());
        }
    }

    abstract protected function getDriver(): BlockStorageInterface;

    abstract public function testOriginBlock(): void;

    abstract public function testCreatingBlocks(): void;

    abstract public function testDeletingBlocks(): void;

    abstract public function testFindingBlocks(): void;

    abstract public function testFindingChildren(): void;

}