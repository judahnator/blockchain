<?php

namespace judahnator\BlockChain;

use judahnator\BlockChain\Exceptions\BlockNotFoundException;
use judahnator\BlockChain\Exceptions\InvalidBlockException;


/**
 * Class Block
 * @property \DateTime $created_at
 * @property int $height
 * @property object $data
 * @property string $hash
 * @property Block|null $previous
 * @property bool $orphaned
 * @package judahnator\BlockChain
 */
final class Block
{

    private $blockHeight, $blockData, $previousBlockHash, $created_at;
    private static $additionalProperties = [];

    /**
     * Block constructor.
     *
     * @param int       $height
     * @param string    $previousHash
     * @param \DateTime $created_at
     * @param           $data
     */
    public function __construct(int $height, string $previousHash, \DateTime $created_at, \stdClass $data)
    {
        $this->blockHeight = $height;
        $this->blockData = $data;
        $this->previousBlockHash = $previousHash;
        $this->created_at = $created_at;
    }

    public function __get($name)
    {
        switch($name) {
            case "created_at":
                return $this->created_at;
            case "height":
                return $this->blockHeight;
            case "data":
                return $this->blockData;
            case "previous":
                if (!$this->previousBlockHash) return null;
                return BlockChain::find($this->previousBlockHash);
            case "hash":
                return $this->calculateHash();
            case "orphaned":
                return $this->blockIsOrphaned();
            default:
                if (array_key_exists($name, self::$additionalProperties)) {
                    return (self::$additionalProperties[$name])($this);
                }
                return null;
        }
    }

    /**
     * Allows the indirect extension of the Block class.
     * All of the functions passed here will have a Block instance passed to them as its single parameter.
     *
     * @param string $name
     * @param callable $function
     */
    public static function addProperty(string $name, callable $function): void
    {
        self::$additionalProperties[$name] = $function;
    }

    /**
     * Determine if this block is orphaned.
     *
     * @return bool
     */
    private function blockIsOrphaned(): bool
    {
        $block = $this;
        do {
            try {
                $block = $block->previous;
            } catch (BlockNotFoundException $e) {
                return true;
            } catch (InvalidBlockException $e) {
                return true;
            }
        } while ($block);
        return false;
    }

    /**
     * Calculates the blocks hash.
     *
     * @return string
     */
    private function calculateHash(): string
    {
        $string = $this->height.'-'.$this->created_at->getTimestamp();
        if (!$this->height === 0) {
            $string .= '-'.$this->previous->hash;
        }
        $string .= '-'.json_encode($this->data);
        return hash('sha256', $string);
    }

    /**
     * Returns the children of this block, optionally recursively.
     *
     * @return array
     */
    public function children(): array
    {
        return BlockChain::children($this);
    }

    /**
     * Creates a child to $this block and returns it.
     *
     * @param \stdClass $data
     * @param bool $addToIndex
     *
     * @param \DateTime|null $created_at
     * @return Block
     */
    public function createChild(\stdClass $data, bool $addToIndex = true, \DateTime $created_at = null): self
    {
        return BlockChain::create($data, $this, $created_at);
    }

    /**
     * Delete the current block.
     */
    public function delete(): void
    {
        BlockChain::delete($this);
    }

    /**
     * Returns true if the property given has already been registered.
     *
     * @param string $property
     * @return bool
     */
    public static function hasProperty(string $property): bool
    {
        return array_key_exists($property, self::$additionalProperties);
    }
}