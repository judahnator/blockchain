<?php

namespace judahnator\BlockChain\Drivers;


use judahnator\BlockChain\Block;
use function judahnator\BlockChain\createOriginBlock;
use judahnator\BlockChain\Exceptions\BlockNotFoundException;
use judahnator\BlockChain\Exceptions\InvalidBlockException;

class MemoryDriver implements BlockStorageInterface
{

    private $blocks = [];

    public function children(Block $block): array
    {
        $blocks = array_filter(
            $this->blocks,
            function(Block $blockToFilter) use ($block): bool
            {
                $previousHash = $blockToFilter->previous ? $blockToFilter->previous->hash : '';
                $heightMatches = $blockToFilter->height === ($block->height + 1);
                $hashMatches = $previousHash === $block->hash;
                return $heightMatches && $hashMatches;
            }
        );
        return array_values($blocks);
    }

    public function delete(Block $block): void
    {
        if (array_key_exists($block->hash, $this->blocks)) {
            unset($this->blocks[$block->hash]);
        }
    }

    public function find(string $blockHash): Block
    {
        if (!array_key_exists($blockHash, $this->blocks)) {
            throw new BlockNotFoundException('The given block could not be found.');
        }
        if ($blockHash !== $this->blocks[$blockHash]->hash) {
            throw new InvalidBlockException('The blocks hash is invalid, it will not be returned.');
        }
        return $this->blocks[$blockHash];
    }

    public function originBlock(\stdClass $defaultData = null): Block
    {
        if (count($this->blocks)) {
            return reset($this->blocks);
        }
        return createOriginBlock($defaultData ?? new \stdClass());
    }

    public function save(Block $block): void
    {
        $this->blocks[$block->hash] = $block;
    }

}