<?php

namespace judahnator\BlockChain\Tests;

use judahnator\BlockChain\Block;
use judahnator\BlockChain\BlockChain;
use judahnator\BlockChain\Drivers\BlockStorageInterface;
use judahnator\BlockChain\Drivers\MemoryDriver;
use judahnator\BlockChain\Exceptions\BlockNotFoundException;

class MemoryDriverTest extends DriverTestCase
{

    protected function getDriver(): BlockStorageInterface
    {
        return new MemoryDriver();
    }

    public function testCreatingBlocks(): void
    {
        $newBlock = $this->blockChain::create((object)['string' => 'foobar'], BlockChain::originBlock());
        $this->assertInstanceOf(Block::class, $newBlock);
        $this->assertEquals('foobar', $newBlock->data->string);
        $this->assertEquals(1, $newBlock->height);
    }

    public function testDeletingBlocks(): void
    {
        $this->expectException(BlockNotFoundException::class);
        $newBlock = BlockChain::create((object)['string' => 'foobar'], BlockChain::originBlock());
        $newBlock->delete();
        BlockChain::find($newBlock->hash);
    }

    public function testOriginBlock(): void
    {
        // Create the first block
        $originBlock = $this->blockChain::originBlock((object)['string' => 'First Block!']);
        $this->assertEquals('First Block!', $originBlock->data->string);

        // Make sure the first block data is immutable
        $originBlock = $this->blockChain::originBlock((object)['string' => 'Data might have changed']);
        $this->assertNotEquals('First Block', $originBlock->data->string);
    }

    public function testFindingBlocks(): void
    {
        $this->assertInstanceOf(Block::class, $this->blockChain::originBlock());
    }

    public function testFindingChildren(): void
    {
        $this->assertTrue(is_array($this->blockChain::originBlock()->children()));
    }
}