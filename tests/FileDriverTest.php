<?php

namespace judahnator\BlockChain\Tests;


use judahnator\BlockChain\Block;
use judahnator\BlockChain\BlockChain;
use judahnator\BlockChain\Drivers\BlockStorageInterface;
use judahnator\BlockChain\Drivers\FileDriver;
use judahnator\BlockChain\Exceptions\BlockNotFoundException;

class FileDriverTest extends DriverTestCase
{

    private $blocksToDelete = [];

    protected function getDriver(): BlockStorageInterface
    {
        return new FileDriver();
    }

    public function testOriginBlock(): void
    {
        $block = $this->blockChain::originBlock();
        $this->assertInstanceOf(Block::class, $block);
        $this->assertEquals('Hello World!', $block->data->string);
    }

    public function testCreatingBlocks(): void
    {
        /** @var Block $origin */
        $origin = $this->blockChain::originBlock();
        $child = $origin->createChild((object)['string' => 'child block']);
        $this->assertInstanceOf(Block::class, $child);
        $this->assertEquals($origin->height + 1, $child->height);
        $this->assertEquals('child block', $child->data->string);
    }

    /**
     * @depends testCreatingBlocks
     */
    public function testDeletingBlocks(): void
    {
        $this->expectException(BlockNotFoundException::class);
        foreach ($this->blockChain::originBlock()->children() as $block) {
            $block->delete();
        }
        $this->blockChain::find($block->hash);
    }

    public function testFindingBlocks(): void
    {
        $originHash = $this->blockChain::originBlock()->hash;
        $this->assertEquals(BlockChain::find($originHash)->hash, $originHash);
    }

    public function testFindingChildren(): void
    {
        $this->assertTrue(is_array($this->blockChain::originBlock()->children()));
    }
}