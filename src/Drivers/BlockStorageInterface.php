<?php

namespace judahnator\BlockChain\Drivers;


use judahnator\BlockChain\Block;

interface BlockStorageInterface
{

    public function children(Block $block): array;

    public function delete(Block $block): void;

    public function find(string $blockHash): Block;

    public function originBlock(\stdClass $defaultData = null): Block;

    public function save(Block $block): void;

}